#ifndef NANO_UTILS_MD5_H
#define NANO_UTILS_MD5_H

struct MD5Context {
    unsigned int buf[4];
    unsigned int bits[2];
    unsigned char in[64];
};



#endif // NANO_UTILS_MD5_H