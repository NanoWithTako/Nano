/**
* @file SingletonPattern.cpp
*/
#include <iostream>

using namespace std;

class Singleton {
    static Singleton s;
    int i;

    Singleton(int x) : i(x) {}

    Singleton &operator=(Singleton &); // Disallowed
    Singleton(const Singleton &); // Disallowed

public:
    static Singleton &instance() { return s; }

    int getValue() { return i; }

    void setValue(int x) { i = x; }
};

Singleton Singleton::s(47);

int main() {
    auto &s = Singleton::instance();
    cout << s.getValue() << endl;
    auto &s2 = Singleton::instance();
    s2.setValue(9);
    cout << s2.getValue() << endl;
}