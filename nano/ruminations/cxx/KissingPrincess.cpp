//
// Created by 郑朝海 on 2022/8/13.
//
#include <iostream>

using namespace std;

class Creature {
    bool isFrog;
public:
    Creature() : isFrog(true) {}

    void greet() const {
        if (isFrog) {
            cout << "Ribbed!" << endl;
        } else {
            cout << "Darling!" << endl;
        }
    }

    void kiss() { isFrog = false; }
};

int main() {
    Creature creature;
    creature.greet();
    creature.kiss();
    creature.greet();
}