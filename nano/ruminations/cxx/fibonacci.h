//
// Created by 郑朝海 on 2022/8/9.
//

#ifndef RUMINATIONS_FIBONACCI_H
#define RUMINATIONS_FIBONACCI_H

int fibonacci(int n);

#endif //RUMINATIONS_FIBONACCI_H
