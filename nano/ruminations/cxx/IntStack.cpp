//
// Created by 郑朝海 on 2022/8/9.
//
#include <iostream>
#include "fibonacci.h"
#include "require.h"

using namespace std;

class IntStack {
    enum {
        ssize = 100
    };

    int stack[ssize]{};
    int top;

public:
    IntStack() : top(0) {}

    void push(int i) {
        require(top < ssize, "Too many push()s");
        stack[top++] = i;
    }

    int pop() {
        require(top > 0, "Too many pop()s");
        return stack[--top];
    }
};

int main() {
    using namespace std;
    IntStack is;
    // Add some Fibonacci numbers, for interest:
    for (int i = 0; i < 20; ++i) {
        is.push(fibonacci(i));
    }

    // Pop & print them:
    for (int i = 0; i < 20; ++i) {
        cout << is.pop() << endl;
    }
}