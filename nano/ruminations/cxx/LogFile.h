//
// Created by 郑朝海 on 2022/8/12.
//

#ifndef RUMINATIONS_LOGFILE_H
#define RUMINATIONS_LOGFILE_H

#include <fstream>

std::ofstream &logfile();

#endif //RUMINATIONS_LOGFILE_H
