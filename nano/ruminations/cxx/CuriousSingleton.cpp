//
// Created by 郑朝海 on 2022/8/13.
//
#include <iostream>

using namespace std;

template<class T>
class Singleton {
public:
    Singleton(const Singleton &) = delete;

    Singleton &operator=(const Singleton &) = delete;

protected:
    Singleton() = default;

    virtual ~Singleton() = default;

public:
    static T &instance() {
        static T theInstance;
        return theInstance;
    }
};

// A sample class to be made into a Singleton
class MyClass : public Singleton<MyClass> {
    int x;
protected:
    friend class Singleton<MyClass>;

    MyClass() { x = 0; }

public:
    void setValue(int n) { x = n; }

    int getValue() const { return x; }
};

int main() {
    MyClass& m = MyClass::instance();
    cout << m.getValue() << endl;
    m.setValue(1);
    cout << m.getValue() << endl;
}