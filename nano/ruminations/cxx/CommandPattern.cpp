//
// Created by 郑朝海 on 2022/8/13.
//
#include <iostream>
#include <vector>

using namespace std;

class Command {
public:
	virtual ~Command() = default;
	virtual void execute() = 0;
};

class Hello: public Command {
public:
    void execute() override { cout << "Hello ";}
};

class World: public Command {
public:
    void execute() override {cout << "World! ";}
};

class IAm: public Command {
public:
    void execute() override {cout << "I'm the command pattern!";}
};

// An object thar holds commands
class Macro {
    vector<Command*> commands;
public:
    void add(Command* c) {commands.push_back(c);}
    void run() {
	    auto it = commands.begin();
        while (it!=commands.end()) {
            (*it++)->execute();
        }
    }
};

int main() {
    Macro macro;
    macro.add(new Hello);
    macro.add(new World);
    macro.add(new IAm);
    macro.run();
}