//
// Created by 郑朝海 on 2022/8/12.
//

#include "UseLog1.h"
#include "LogFile.h"

using namespace std;

void g() {
    logfile() << __FILE__ << endl;
}

int main() {
    f();
    g();
}