//
// Created by 郑朝海 on 2022/8/13.
//
// Simple demonstration of the Proxy pattern.
#include <iostream>

using namespace std;

class ProxyBase {
public:
    virtual void f() = 0;

    virtual void g() = 0;

    virtual void h() = 0;

    virtual ~ProxyBase() = default;
};

class Implementation : public ProxyBase {
public:
    void f() override {
        cout << "Implementation.f()" << endl;
    }

    void g() override {
        cout << "Implementation.g()" << endl;
    }

    void h() override {
        cout << "Implementation.h()" << endl;
    }
};

class Proxy : public ProxyBase {
    ProxyBase *implementation;
public:
    Proxy() { implementation = new Implementation(); }

    ~Proxy() override { delete implementation; }

    // Forward calls to the implementation:
    void f() override {
        implementation->f();
    }

    void g() override {
        implementation->g();
    }

    void h() override {
        implementation->h();
    }
};

int main() {
    Proxy p;
    p.f();
    p.g();
    p.h();
}