//
// Created by 郑朝海 on 2022/8/9.
//

#ifndef RUMINATIONS_SURROGATE_H
#define RUMINATIONS_SURROGATE_H

class Vehicle {
public:
    virtual double weight() const = 0;

    virtual void start() = 0;

    virtual Vehicle* copy() const = 0;

    virtual ~Vehicle() {}
    // ...
};

class RoadVehicle : public Vehicle {
    /* ... */
};

class AutoVehicle : public RoadVehicle {
    /* ... */
};

class Aircraft : public Vehicle {
    /* ... */
};

class Helicopter : public Aircraft {
    /* ... */
};

#endif //RUMINATIONS_SURROGATE_H
