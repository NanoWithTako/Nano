//
// Created by 郑朝海 on 2022/8/13.
//

#ifndef RUMINATIONS_P4SOLUTION_H
#define RUMINATIONS_P4SOLUTION_H

class P4Solution final {
public:
    inline static double pow(double x, int n) {
        double ret = 1;
        for (int i = 0; i < n; ++i) {
            ret *= x;
        }
        return ret;
    }
};

#endif //RUMINATIONS_P4SOLUTION_H
