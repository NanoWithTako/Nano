//
// Created by 郑朝海 on 2022/8/14.
//

#ifndef RUMINATIONS_P6SOLUTION_H
#define RUMINATIONS_P6SOLUTION_H

class P6Solution final {
public:
    inline static bool isPowerOf(int n, int base) {
        if (n < 0) {
            return false;
        }
        if (n ==1) {
            return true;
        }
        if (n < base) {
            return false;
        }
        long long ans = 1;
        while (true) {
            ans *= base;
            if (ans > n) {
                return false;
            } else if (ans == n) {
                return true;
            }
        }
        return false;
    }
};

#endif //RUMINATIONS_P6SOLUTION_H
