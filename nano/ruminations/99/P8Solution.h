//
// Created by 郑朝海 on 2022/8/14.
//

#ifndef RUMINATIONS_P8SOLUTION_H
#define RUMINATIONS_P8SOLUTION_H

#include <cmath>

class P8Solution final {
public:
    inline static int multiply(int a, int b) {
        int base = std::fmax(a, b);
        int loop = std::fmin(a, b);
        if (loop == 0)
            return 0;
        // a * b = (a - 1 + 1) * b = (a - 1) * b + b
        return multiply(loop - 1, base) + base;
    }
};

#endif //RUMINATIONS_P8SOLUTION_H
