//
// Created by 郑朝海 on 2022/8/13.
//
#include <iostream>
#include "P1Solution.h"
#include "P2Solution.h"
#include "P3Solution.h"
#include "P4Solution.h"
#include "P5Solution.h"
#include "P6Solution.h"
#include "P7Solution.h"
#include "P8Solution.h"

using namespace std;

int main() {
    {
        /// 1 Function
        cout << P1Solution::func<int>(1, 2) << endl;
    }

    {
        /// 2 library function
        cout << P2Solution::sqrt(10) << " " << P2Solution::sqrt2(11) << endl;
    }

    {
        /// 3
        cout << P3Solution::isPerfectSquare(4) << " " << P3Solution::isPerfectSquare(10) << endl;
    }

    {
        /// 4
        cout << pow(3, 100) << " " << P4Solution::pow(3, 100) << endl;
    }

    {
        /// 5
        cout << P5Solution::isPowerOfTwo(4) << " "
             << P5Solution::isPowerOfTwo((int) pow(2, 15)) << endl;
    }

    {
        /// 6
        cout << P6Solution::isPowerOf(4, 5) << " "
             << P6Solution::isPowerOf(4, 3) << " "
             << P6Solution::isPowerOf(4, 4) << endl;
    }

    {
        /// 7
        auto numbers = vector<int>{3, 4};
        const auto nums = P7Solution::swap(numbers);
        cout << nums[0] << " " << nums[1] << endl;
    }

    {
        /// 8
        cout << P8Solution::multiply(3, 4) << endl;
    }
}
