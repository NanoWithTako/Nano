//
// Created by 郑朝海 on 2022/8/14.
//

#ifndef RUMINATIONS_P7SOLUTION_H
#define RUMINATIONS_P7SOLUTION_H

#include <vector>

using namespace std;

class P7Solution final {
public:
    inline static vector<int> swap(vector<int>& numbers) {
        numbers[0] = numbers[0] * numbers[1];
        numbers[1] = numbers[0] / numbers[1];
        numbers[0] = numbers[0] / numbers[1];
        return numbers;
    }
};
#endif //RUMINATIONS_P7SOLUTION_H
