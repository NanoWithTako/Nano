//
// Created by 郑朝海 on 2022/8/14.
//

#ifndef RUMINATIONS_P5SOLUTION_H
#define RUMINATIONS_P5SOLUTION_H

class P5Solution final {
public:
    inline static bool isPowerOfTwo(int n) {
        if (n < 0) {
            return false;
        }
        if (n ==1) {
            return true;
        }
        long long ans = 1;
        while (true) {
            ans *= 2;
            if (ans > n) {
                return false;
            } else if (ans == n) {
                return true;
            }
        }
        return false;
    }
};
#endif //RUMINATIONS_P5SOLUTION_H
