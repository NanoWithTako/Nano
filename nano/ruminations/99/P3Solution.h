//
// Created by 郑朝海 on 2022/8/13.
//

#ifndef RUMINATIONS_P3SOLUTION_H
#define RUMINATIONS_P3SOLUTION_H

class P3Solution final {
public:
    inline static bool isPerfectSquare(int num) {
        for (int i = 1; i < num ; ++i) {
            if (i * i > num) {
                return false;
            } else if (i * i == num) {
                return true;
            }
        }
        return false;
    }
};

#endif //RUMINATIONS_P3SOLUTION_H
