//
// Created by 郑朝海 on 2022/8/13.
//

#ifndef RUMINATIONS_FUNCTION_H
#define RUMINATIONS_FUNCTION_H

class P1Solution final {
public:
    template<class T>
    static T func(T a1, T a2) {
        return a1 + a2;
    }
};

#endif //RUMINATIONS_FUNCTION_H
