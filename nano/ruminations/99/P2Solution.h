//
// Created by 郑朝海 on 2022/8/13.
//

#ifndef RUMINATIONS_P2SOLUTION_H
#define RUMINATIONS_P2SOLUTION_H

#include <cmath>

class P2Solution final {
public:
    inline static int sqrt(int x) {
        return (int) ::sqrt(x);
    }

    inline static int sqrt2(int x) {
        int last = 1;
        for (int i = 1; i < x ; ++i) {
            if (i * i > x) {
                return last;
            } else if (i * i == x) {
                return i;
            } else {
                last = i;
            }
        }
        return last;
    }
};

#endif //RUMINATIONS_P2SOLUTION_H
