/// @file nano.h
///
/// @brief
///
/// @since 2022/8/6

#include <stdio.h>
#include <stdlib.h> // for EXIT_SUCCESS
#include <assert.h>


int main(int argc, char **argv) {
    printf("Hello, World!\n");
    assert(argc > 2);
    char *p = calloc(1, sizeof(char));
    free(p);
    return EXIT_SUCCESS;
}