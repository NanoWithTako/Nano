package com.example.design.creational;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

// Use static Factory:
public class VehicleFactory {
    private Map<String, Class<Vehicle>> registeredProducts = new HashMap<String, Class<Vehicle>>();

    public enum VehicleType {
        Bike, Car, Truck;
    }

    public static Vehicle create(VehicleType type) {
        if (type.equals(VehicleType.Bike)) {
            return new Bike();
        } else if (type.equals(VehicleType.Car)) {
            return new Car();
        } else if (type.equals(VehicleType.Truck)) {
            return new Truck();
        } else {
            return null;
        }
    }

    public void registerVehicle(String vehicleId, Class<Vehicle> vehicleClass) {
        registeredProducts.put(vehicleId, vehicleClass);
    }

    public Vehicle createVehicle(String type) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<Vehicle> productClass = registeredProducts.get(type);
        return productClass.getDeclaredConstructor().newInstance();
    }
}
