package com.example.design.creational;

public class LockFreeSingleton extends DoSomethingBase {
    private static final LockFreeSingleton instance = new LockFreeSingleton();

    private LockFreeSingleton() {
        System.out.printf("%s is Instantiated\n", LockFreeSingleton.class.getSimpleName());
    }

    public static synchronized LockFreeSingleton getInstance() {
        return instance;
    }
}
