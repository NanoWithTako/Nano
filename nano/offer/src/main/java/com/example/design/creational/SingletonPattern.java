package com.example.design.creational;

public class SingletonPattern {
    private static SingletonPattern instance;

    public static SingletonPattern getInstance() {
        if (instance == null) {
            instance = new SingletonPattern();
        }
        return instance;
    }

    public void run() {
        System.out.println("do something...");
    }

    private SingletonPattern() {
        System.out.println("SingletenPattern create");
    }
}
