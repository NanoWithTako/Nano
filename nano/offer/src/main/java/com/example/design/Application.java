package com.example.design;

import com.example.design.creational.*;

public class Application {
    public static void run() {
        SingletonPattern.getInstance().run();

        {
            System.out.println("==============================");
            Singleton.getInstance().doSomething();
        }

        {
            System.out.println("==============================");
            SyncSingleton.getInstance().doSomething();
        }

        {
            System.out.println("==============================");
            SyncSingleton2.getInstance().doSomething();
        }

        {
            System.out.println("==============================");
            SyncSingleton3.getInstance().doSomething();
        }

        {
            System.out.println("==============================");
            LockFreeSingleton.getInstance().doSomething();
        }
    }
}
