package com.example.design.creational;

public class SyncSingleton2 extends DoSomethingBase {
    private static SyncSingleton2 instance;
    private SyncSingleton2() {
        System.out.println("SyncSingleton2 is Instantiated.");
    }

    public static SyncSingleton2 getInstance() {
        synchronized (SyncSingleton2.class) {
            if (instance == null) {
                instance = new SyncSingleton2();
            }
        }
        return instance;
    }
}
