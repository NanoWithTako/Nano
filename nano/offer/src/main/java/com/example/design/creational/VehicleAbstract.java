package com.example.design.creational;

public abstract class VehicleAbstract {
    abstract public Vehicle newInstance();
}
