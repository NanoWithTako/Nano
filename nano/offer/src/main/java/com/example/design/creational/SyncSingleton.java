package com.example.design.creational;

public class SyncSingleton extends DoSomethingBase  {
    private static SyncSingleton instance;
    private SyncSingleton() {
        System.out.println("SyncSingleton is Instantiated.");
    }

    public static synchronized SyncSingleton getInstance() {
        if (instance == null) {
            instance = new SyncSingleton();
        }
        return instance;
    }
}
