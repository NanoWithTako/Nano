package com.example.design.creational;

public class SyncSingleton3 extends DoSomethingBase {
    private static SyncSingleton3 instance;

    private SyncSingleton3() {
        System.out.println("SyncSingleton3 is Instantiated.");
    }

    public static SyncSingleton3 getInstance() {
        if (instance == null) {
            synchronized (SyncSingleton3.class) {
                if (instance == null) {
                    instance = new SyncSingleton3();
                }
            }
        }
        return instance;
    }
}
