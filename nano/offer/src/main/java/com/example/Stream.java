package com.example;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Stream {

    private List<Integer> doSetCreate() {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        return list;
    }

    private List<Integer> doOddsCreate(List<Integer> list) {
        List<Integer> odds = new ArrayList<>();
        for (int val : list) {
            if (val % 2 == 0) {
                odds.add(val);
            }
        }
        return odds;
    }

    public void run() {
        for (int val : doOddsCreate(doSetCreate())) {
            System.out.print(val);
        }
    }

    public void useStream() {
        IntStream.range(0, 10).filter(i -> i % 2 == 0).forEach(System.out::print);
    }
}
