package com.example;

import com.example.design.Application;

/**
 * Hello world!
 */
public final class App {
    private App() {
        Application.run();
    }

    /**
     * Says hello to the world.
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
        new App();

        {
            System.out.println("==============================");
            Stream stream = new Stream();
            stream.run();
            System.out.println();
            stream.useStream();
        }
    }
}
